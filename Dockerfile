FROM ubuntu:20.04

WORKDIR /src
ADD https://github.com/aquasecurity/trivy/releases/download/v0.18.3/trivy_0.18.3_Linux-64bit.deb .
RUN dpkg -i trivy_0.18.3_Linux-64bit.deb

ENTRYPOINT [""]
